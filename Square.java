package motion_test;

import lejos.hardware.motor.Motor;
import lejos.robotics.navigation.DifferentialPilot;
 
/**
 * Robot travels around a square
 * @author Natalia Chilikina
 * 
 */

public class Square {
  DifferentialPilot pilot;
  
  private void goSquare(){
	  //We should repeat square 10 times 
	  for(int i=0; i<10; i++){
		  pilot.travel(-39.37f);         
		  pilot.rotate(-90);        
		  pilot.travel(-31.5f);  
		  pilot.rotate(-90);       
		  pilot.travel(-39.37f);   
		  pilot.rotate(-90);       
		  pilot.travel(-31.5f);  
		  pilot.rotate(-90);     
	  }
	  pilot.stop();
  }
  

  
  public static void main(String[] args) {
    Square traveler = new Square();
    traveler.pilot = new DifferentialPilot(1.97f, 5.40f, Motor.B, Motor.C);
    traveler.goSquare(); 
  }

}