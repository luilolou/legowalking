package motion_test;

import lejos.hardware.Button;
import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.hardware.motor.Motor;
import lejos.hardware.port.MotorPort;
import lejos.robotics.RegulatedMotor;
import lejos.robotics.navigation.DifferentialPilot;
 
/**
 * Robot travels round with different speeds
 */
public class Round {
  static RegulatedMotor mB;
  static RegulatedMotor mC;
  private final static float PI = 3.14159265359f;
  static float WHEEL_DISTANCE = 5.40f;
  public final static float CM_TO_MOTOR_ANGLE = 14.7f;
  

  
  public static void turnRight(int degree, boolean both, float radius) {
		float distanceFullCircle = 2 * PI * radius;
		float distanceToMove = distanceFullCircle / 360.0f * degree;

		float distanceOneRotation = 1.97f * PI;

		float amountRotations = distanceToMove / distanceOneRotation;
		int degreesToRotate = (int) (amountRotations * 360.0f);
		mB.setSpeed(300);
		mC.setSpeed(150);
		
		mB.startSynchronization();
		mB.rotate(degreesToRotate, true);
		if (both) {
			mC.rotate(degreesToRotate, true);
		}
		mB.endSynchronization();
		
	}

  
  private static void goRound(double speed, double d) {
	    mB.setSpeed(300);
	    mC.setSpeed(300);

		mB.startSynchronization();
		mB.forward();
		mC.forward();
		mB.endSynchronization();
  }
  
  public static void main(String[] args) {
    mB = new EV3LargeRegulatedMotor(MotorPort.B);
    mC = new EV3LargeRegulatedMotor(MotorPort.C);
    mB.synchronizeWith(new RegulatedMotor[] {mC});
    
    turnRight(360, true, 5f);
    //goRound(0.41,39.37f); 
    //traveler.goRound(10, 0.82); 
    //traveler.goRound(10, 1.33); 
  }

}