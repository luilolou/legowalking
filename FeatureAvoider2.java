package motion_test;

import lejos.hardware.Button;
import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.hardware.port.MotorPort;
import lejos.hardware.port.SensorPort;
import lejos.hardware.sensor.EV3UltrasonicSensor;
import lejos.robotics.RegulatedMotor;
 
 
public class FeatureAvoider2 {
     
    public static void main(String[] args) {    
        RegulatedMotor mB = new EV3LargeRegulatedMotor(MotorPort.B);
        RegulatedMotor mC = new EV3LargeRegulatedMotor(MotorPort.C);
        mB.synchronizeWith(new RegulatedMotor[] {mC});
        
        EV3UltrasonicSensor us = new EV3UltrasonicSensor(SensorPort.S3);
        
        mB.setSpeed(200);
        mC.setSpeed(200);

        float[] value = new float[1];
		while (!Button.ESCAPE.isDown()) {
			us.getDistanceMode().fetchSample(value, 0);
			System.out.println(value[0]);
			if(value[0] != 0.0f)	{
				if (value[0] > 0.85) {
					mB.startSynchronization();
					mB.stop();
					mC.stop();
					mB.endSynchronization();	
				}
				else if (value[0] > 0.25) {
					mB.startSynchronization();
					mB.forward();
					mC.forward();
					mB.endSynchronization();
				}
				else if ((value[0] >= 0.01) && (value[0] <= 0.15)) {
					mB.startSynchronization();
					mB.backward();
					mC.backward();
					mB.endSynchronization();
				}
				else {
					mB.startSynchronization();
					mB.stop();
					mC.stop();
					mB.endSynchronization();
				}
			}
			else {
				mB.startSynchronization();
				mB.stop();
				mC.stop();
				mB.endSynchronization();
			}	
		}
		mB.stop();
		mC.stop();
		us.close();
    }
}