package motion_test;

import lejos.hardware.motor.Motor;
import lejos.robotics.navigation.DifferentialPilot;
 
/**
 * Robot draws a triangle
 * @author Natalia Chilikina
 * 
 */

public class Competition {
  DifferentialPilot pilot;
  
  private void goBackSquare(){
	  //We should repeat square 10 times 
	  for(int i=0; i<5; i++){
		  pilot.travel(-41.0f);         
		  pilot.rotate(-53);       
		  pilot.travel(27.55f);   
		  pilot.rotate(100);       
		  pilot.travel(31.5f);  
		  pilot.rotate(-44);        
	  }
	  pilot.stop();
  }

  private void goSquare(){
	  //We should repeat square 10 times 
	  for(int i=0; i<5; i++){
		  pilot.travel(39.37f);         
		  pilot.rotate(180-44);        
		  pilot.travel(31.5f);  
		  pilot.rotate(180-83);       
		  pilot.travel(27.55f);   
		  pilot.rotate(180-53);       
	  }
	  pilot.stop();
  }
  

  
  public static void main(String[] args) {
    Competition traveler = new Competition();
    traveler.pilot = new DifferentialPilot(1.97f, 5.40f, Motor.B, Motor.C);
    traveler.goBackSquare(); 
  }

}