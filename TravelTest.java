package motion_test;

import lejos.hardware.Button;
import lejos.hardware.ev3.LocalEV3;
import lejos.hardware.motor.Motor;
import lejos.hardware.port.Port;
import lejos.hardware.port.SensorPort;
import lejos.hardware.sensor.*;
import lejos.robotics.RangeFinder;
//import lejos.hardware.sensor.
import lejos.robotics.RangeFinderAdapter;
import lejos.robotics.SampleProvider;
import lejos.robotics.navigation.ArcMoveController;
import lejos.robotics.navigation.DifferentialPilot;
import lejos.robotics.navigation.MoveController;
import lejos.robotics.objectdetection.Feature;
import lejos.robotics.objectdetection.FeatureDetector;
import lejos.robotics.objectdetection.FeatureListener;
import lejos.robotics.objectdetection.RangeFeatureDetector;
 
/**
 * Robot travels to obstacle and back again
 */
public class TravelTest {
  DifferentialPilot pilot;
  EV3IRSensor ir = new EV3IRSensor(SensorPort.S4);
  SampleProvider bump = ir.getDistanceMode();
  float[] sample = new float[1];
  static final float MAX_DISTANCE = 50f;
  static final int DETECTOR_DELAY = 1000;

 
  public void go() {
	    pilot.forward();
	    while (pilot.isMoving()) {
	      bump.fetchSample(sample, 0);
	      if (sample[0] < 20) pilot.stop();
	    }
	    float dist = pilot.getMovement().getDistanceTraveled();
	    System.out.println("Distance = " + dist);
	    pilot.travel(-dist);
	  }
  
  /**
   * Go around
   */
  private void goRound(double speed, double d) {
	  pilot.arc(d, 360);
	 // pilot.setRotateSpeed(speed);
	    while (pilot.isMoving()) {
	      bump.fetchSample(sample, 0);
	      if (sample[0] < 5) pilot.stop();
	    }
	  }
	 
  
  private void goSquare(){
	  for(int i=0; i<10; i++){
		  pilot.travel(-39.37f);         // cm
		  pilot.rotate(-90);        // degree clockwise
		  pilot.travel(-31.5f);  //  move backward for 50 cm
		  pilot.rotate(-90);        // degree clockwise
		  pilot.travel(-39.37f);         // cm
		  pilot.rotate(-90);        // degree clockwise
		  pilot.travel(-31.5f);  //  move backward for 50 cm
		  pilot.rotate(-90);        // degree clockwise  
	  }
	 
	  pilot.stop();
  }
  

  
  public static void main(String[] args) {
    TravelTest traveler = new TravelTest();
    traveler.pilot = new DifferentialPilot(1.97f, 5.40f, Motor.B, Motor.C);
    //traveler.go();
    //traveler.goSquare(); 
    //traveler.folow();
    //traveler.goRound(0.41,39.37f); 
    //traveler.goRound(10, 0.82); 
    //traveler.goRound(10, 1.33); 
  }

}