package motion_test;

import lejos.hardware.Button;
import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.hardware.port.MotorPort;
import lejos.hardware.port.SensorPort;
import lejos.hardware.sensor.EV3IRSensor;
import lejos.robotics.RegulatedMotor;
 
 
public class FollowLine {
     
    public static void main(String[] args) {  
    	// Take motors and sync them
    	// We need synchronization because the wheels are starting and stopping at a different
    	// time and we have an unexpected arc at the end of moving
    	
        RegulatedMotor mB = new EV3LargeRegulatedMotor(MotorPort.B);
        RegulatedMotor mC = new EV3LargeRegulatedMotor(MotorPort.C);
        mB.synchronizeWith(new RegulatedMotor[] {mC});
        
        EV3IRSensor ir = new EV3IRSensor(SensorPort.S4);
        
        //Set speed of motors because default is very quick
        mB.setSpeed(300);
        mC.setSpeed(300);

        float[] value = new float[1];
		while (!Button.ESCAPE.isDown()) {
			// Take distance
			ir.getDistanceMode().fetchSample(value, 0);
			System.out.println(value[0]);
			if(value[0] != 0.0f)	{
				// A point of too long distance
				if (value[0] > 0.85) {
					mB.startSynchronization();
					mB.stop();
					mC.stop();
					mB.endSynchronization();	
				}
				// A point to follow
				else if (value[0] > 0.25) {
					mB.startSynchronization();
					mB.forward();
					mC.forward();
					mB.endSynchronization();
				}
				// A point to get back because too close
				else if ((value[0] >= 0.01) && (value[0] <= 0.15)) {
					mB.startSynchronization();
					mB.backward();
					mC.backward();
					mB.endSynchronization();
				}
				// A point to stop - from 0.15 to 0.25 because if we take exact point - 
				// it cannot get to it and starts to shuffle
				else {
					mB.startSynchronization();
					mB.stop();
					mC.stop();
					mB.endSynchronization();
				}
			}
			else {
				// If nothing happens - we are standing
				mB.startSynchronization();
				mB.stop();
				mC.stop();
				mB.endSynchronization();
			}	
		}
		mB.stop();
		mC.stop();
		ir.close();
    }
}